# Use a imagem do Node.js como base
# FROM node:20-alpine AS build
FROM hub.estaleiro.serpro/pipeline/node:20-debian AS build

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

FROM nginx:latest

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=build /app/dist/browser/* /usr/share/nginx/html/

EXPOSE 80
