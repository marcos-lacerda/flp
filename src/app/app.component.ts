import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Pessoa, TreeComponent } from './tree/tree.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [HttpClientModule, CommonModule, TreeComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
})
export class AppComponent implements OnInit {
  pessoas!: Pessoa[];
  private mapVars: Map<string, Pessoa> = new Map();
  id = 1;
  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.http
      .get('assets/tree.txt', { responseType: 'text' })
      .subscribe((data) => this.receiveTree(data));
  }

  private receiveTree(data: string): void {
    const pessoas: Pessoa[]=[];
    const linhas = data.split('\n');

    const mapPais = new Map<number,Pessoa>();
    let oldLevel=-1;
    for (let linha of linhas) {
      if (!linha) continue;

      const pessoa = this.parsePessoa(linha);
      const level = this.calcLevel(linha);

      if (level!=oldLevel) {
        mapPais.set(level, pessoa);
      }

      if (level==0) {
        pessoas.push(pessoa);
      } else {
        const pai = mapPais.get(level-1)!;
        pessoa.pai = pai;
        pai.filhos.push(pessoa);
      }
    }
    this.expandirVariaveis(pessoas);
    this.pessoas = pessoas;
  }

  private expandirVariaveis(pessoas: Pessoa[]) {
    for (const pessoa of pessoas) {
      const match = /#([^@]+)@/.exec(pessoa.detalhes);
      if (match) {
        const varName = match[1];
        if (this.mapVars.has(varName)) {
          pessoa.filhos = [...this.mapVars.get(varName)!.filhos];
        } else {
          this.mapVars.set(varName, pessoa);
        }
      }
      pessoa.detalhes = pessoa.detalhes.replace(/#[^@]+@/g, '');
      if (pessoa.filhos) {
        this.expandirVariaveis(pessoa.filhos);
      }
    }
  }

  parsePessoa(linha: string): Pessoa {
    linha = linha.replace(/\t/g,'');
    return {
      id: this.id++,
      name: this.obterShortName(linha),
      detalhes: this.obterDetalhes(linha),
      filhos: []
    }
  }

  obterDetalhes(linha: string) {
    return linha.replace(/;/g,' e ').replace(/,/g,' ');
  }

  obterShortName(linha: string) {
    const partes = linha.split(';');
    return partes.map(n => this._obterShortName(n)).join(' e ');
  }

  _obterShortName(nome: string): string {
    const nick = /^.+\((.+)\)/.exec(nome);
    if (nick?.length) return nick[1];

    const partes = nome.split(',')[0].split(' ');
    if (partes.length>1) {
      if (partes[1].length<3) {
        return partes[0] + ' ' + partes[1] + ' ' + partes[2];
      }
      return partes[0] + ' ' + partes[1];
    }
    return partes[0];
  }

  private calcLevel(s: string): number {
    return s.match(/\t/g)?.length ?? 0;
  }



}
