import { NestedTreeControl } from '@angular/cdk/tree';
import { CommonModule } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatTreeModule, MatTreeNestedDataSource } from '@angular/material/tree';

export interface Pessoa {
  pai?: Pessoa;
  id: number;
  name: string;
  detalhes: string;
  filhos: Pessoa[];
}

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrl: './tree.component.css',
  standalone: true,
  imports: [MatTreeModule, MatButtonModule, MatIconModule, CommonModule, MatInputModule, MatCheckboxModule, FormsModule
  ],
})
export class TreeComponent implements OnInit {
  @Input() pessoas!: Pessoa[]
  termo = '';
  searchPersonId = -1;
  pessoaEncontrada: Pessoa | null = null;

  recursive = false;

  treeControl = new NestedTreeControl<Pessoa>(
    (pessoa: Pessoa) => pessoa.filhos
  );

  dataSource = new MatTreeNestedDataSource<Pessoa>();

  ngOnInit(): void {
    this.dataSource.data = this.pessoas;
  }

  hasChild = (_: number, node: Pessoa) => node.filhos.length > 0;

  filterChanged() {
    this.searchPersonId = -1;
    if (this.termo?.length < 3 ) {
      return;
    }
    this.procurarPessoa();
  }

  private procurarPessoa() {
    this.pessoaEncontrada = null;
    this.procurar(this.pessoas, this.termo.toLowerCase());
    this.mostrarPessoa();
  }

  proximo() {
    if (!this.termo || this.searchPersonId == -1) {
      return;
    }
    this.searchPersonId++;
    this.procurarPessoa();
    if (!this.pessoaEncontrada) {
      this.searchPersonId = -1;
      this.procurarPessoa();
    }
  }

  mostrarPessoa() {
    if (this.pessoaEncontrada) {
      this.expandParents(this.pessoaEncontrada);
      this.scrollToPerson(this.pessoaEncontrada);
    }
  }
  scrollToPerson(pessoaEncontrada: Pessoa, i=0) {
    const el = document.getElementById('p-' + this.pessoaEncontrada!.id);
    if (!el) {
      if (i > 10) {
        console.warn('Elemento não encontrado');
        return;
      }
      setTimeout(() => {
        this.scrollToPerson(pessoaEncontrada, i+1);
      });
    } else {
      el.scrollIntoView({ behavior: 'smooth'});
    }
  }

  expandParents(searchPerson: Pessoa) {
    let parent = searchPerson.pai;
    while (parent) {
      this.treeControl.expand(parent);
      parent = parent.pai;
    }
  }

  procurar(pessoas: Pessoa[], termo: string) {
    for (const pessoa of pessoas) {
      if (pessoa.id > this.searchPersonId && pessoa.name.toLowerCase().includes(termo)) {
        this.searchPersonId = pessoa.id;
        this.pessoaEncontrada = pessoa;
        return;
      }
      if (!this.pessoaEncontrada && pessoa.filhos.length > 0) {
        this.procurar(pessoa.filhos, termo);
      }
    }
  }

}
